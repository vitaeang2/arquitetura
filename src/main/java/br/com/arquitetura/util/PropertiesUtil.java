package br.com.arquitetura.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;


/**
 * Classe de conex�o com arquivo de mensagem .properties
 * 
 * @author sergio
 * 
 */
public class PropertiesUtil {

	private static Properties properties;

	public static void loadProperties(ServletContext sc, Locale locale)
			throws FileNotFoundException, IOException {
		if (properties == null) {
			// Busca linguagem local
//			String language = Locale.getDefault().getLanguage();

			String nomeArquivo = "environment.properties";

			String fs = File.separator;
			String file = sc.getRealPath(fs + "WEB-INF" + fs + "classes" + fs
					+ "resources" + fs + "" + nomeArquivo);

			properties = new Properties();
			properties.load(new FileInputStream(file));
		}
	}

	public static String pegarString(String key) {
		return properties.getProperty(key);
	}

	public static String pegarMessageResourceString(String key, Object params[]) {
		String text = null;
		try {
			text = pegarString(key);
		} catch (Exception e) {
			text = key;
		}
		if (params != null) {
			MessageFormat mf = new MessageFormat(text,
					new Locale("pt", "pt_br"));
			text = mf.format(params, new StringBuffer(), null).toString();
		}
		return text;
	}

	protected static ClassLoader getCurrentClassLoader(Object defaultObject) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		if (loader == null) {
			loader = defaultObject.getClass().getClassLoader();
		}
		return loader;
	}

	/**
	 * Retorna o valor da chave passada
	 */
	public static String getMessageResourceString(String key) {
		return getMessageResourceString(key, null);
	}

	public static String getMessageResourceString(String key, Object params[]) {
		String text = null;
		final ResourceBundle bundle = ResourceBundle
				.getBundle(UTF8PropertyMessageBundle.BUNDLE_NAME);
		try {
			text = bundle.getString(key);
		} catch (final Exception e) {
			text = key;
		}
		if (params != null) {
			final MessageFormat mf = new MessageFormat(text, bundle.getLocale());
			text = mf.format(params, new StringBuffer(), null).toString();
		}
		return text;
	}

	private static Locale getLocale(FacesContext context) {
		if (context.getViewRoot() != null
				&& context.getViewRoot().getLocale() != null)
			return context.getViewRoot().getLocale();

		return new Locale("pt", "pt_br");
	}

	public static FacesMessage getMessageResource(String key, Object params[]) {
		String text = null;

		FacesContext context = FacesContext.getCurrentInstance();
		String bundleName = context.getApplication().getMessageBundle();
		Locale locale = context.getViewRoot().getLocale();

		ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale,
				getCurrentClassLoader(params));
		try {
			text = bundle.getString(key);
		} catch (MissingResourceException e) {
			text = "?? key " + key + " n�o encontrada ??";
		}
		if (params != null) {
			MessageFormat mf = new MessageFormat(text, locale);
			text = mf.format(params, new StringBuffer(), null).toString();
		}
		return new FacesMessage(text, null);
	}
}
