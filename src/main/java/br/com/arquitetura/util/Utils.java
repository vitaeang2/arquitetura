package br.com.arquitetura.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.swing.text.MaskFormatter;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import br.com.arquitetura.entidade.Entidade;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class Utils {

	public static boolean isTrue(final Boolean bool) {
		return BooleanUtils.isTrue(bool);
	}

	public static boolean isFalse(final Boolean bool) {
		return !isTrue(bool);
	}

	public static boolean isNotEmpty(final Object objeto) {
		return !isEmpty(objeto);
	}

	public static boolean isEmpty(final Object objeto) {
		if (objeto == null) {
			return true;
		}
		if ((objeto instanceof Collection)) {
			return CollectionUtils.isEmpty((Collection) objeto);
		}
		if ((objeto instanceof Map)) {
			return MapUtils.isEmpty((Map) objeto);
		}
		if ((objeto instanceof String)) {
			return StringUtils.isEmpty((String) objeto);
		}
		if ((objeto instanceof Object[])) {
			return ((Object[]) objeto).length == 0;
		}
		throw new RuntimeException("Tipo de objeto [" + objeto.getClass() + "] não e suportado");
	}
	
	public static void main(String[] args) {

		System.out.println(md5("123"));
	}

	public static String md5(String senha) {
		MessageDigest algorithm;
		try {
			algorithm = MessageDigest.getInstance("MD5");
			final BigInteger hash = new BigInteger(1, algorithm.digest(senha.getBytes()));
			senha = hash.toString(16);
		} catch (final NoSuchAlgorithmException exception) {
			exception.printStackTrace();
		}
		return senha;
	}

	public static String SHA(String senha) throws UnsupportedEncodingException {
		try {
			MessageDigest messageDiegest = MessageDigest.getInstance("SHA-1");
			messageDiegest.update(senha.getBytes("UTF-8"));
			return Base64.encodeBase64String(messageDiegest.digest());
		} catch (NoSuchAlgorithmException e) {
			throw new Error(e);
		} catch (UnsupportedEncodingException e) {
			throw new Error(e);
		}
	}

	public static String removeMascaraCPF(String cpf) {
		if (cpf != null && !cpf.equals("")) {
			return cpf.replace(".", "").replace("-", "");
		}

		return cpf;
	}

	public static String removeMascaraCNPJ(String cpf) {
		if (cpf != null && !cpf.equals("")) {
			return cpf.replace(".", "").replace("/", "").replace("-", "");
		}

		return cpf;
	}

	// public static String generatorPassword()
	// {
	// return encrypt(RandomUtils.mix(6));
	// }
	//
	// public static String encrypt(String texto)
	// {
	// return CriptoUtils.encrypt(texto);
	// }
	//
	// public static String decrypt(String codigo)
	// {
	// return CriptoUtils.decrypt(codigo);
	// }

	public static String beanAlias(final Class<?> clazz) {
		return BeanUtils.beanAlias(clazz);
	}

	public static <T extends Entidade> T setValueTO(final Object id, final Class<T> to) {
		try {
			if ((id != null) && (isNotEmpty(id.toString()))) {
				final Entidade obj = to.newInstance();
				org.apache.commons.beanutils.BeanUtils.setProperty(obj, "id", id);
				return (T) obj;
			}

			return null;
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String formatMoedas(final Double valor) {
		final Locale BRAZIL = new Locale("pt", "BR");
		final DecimalFormatSymbols REAL = new DecimalFormatSymbols(BRAZIL);
		final DecimalFormat DINHEIRO_REAL = new DecimalFormat("¤ ###,###,##0.00", REAL);
		return DINHEIRO_REAL.format(valor);
	}

	public static String getRandomPass(int len) {
		char[] chart = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
				'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
				'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
				'Z' };

		char[] senha = new char[len];

		int chartLenght = chart.length;
		Random rdm = new Random();

		for (int x = 0; x < len; x++)
			senha[x] = chart[rdm.nextInt(chartLenght)];

		return new String(senha);
	}

	public static String formatCPF(String cpf) {
		return format("###.###.###-##", cpf);
	}

	public static String formatCNPJ(String cnpj) {
		return format("##.###.###/####-##", cnpj);
	}

	private static String format(String pattern, Object value) {
		MaskFormatter mask;
		try {
			mask = new MaskFormatter(pattern);
			mask.setValueContainsLiteralCharacters(false);
			return mask.valueToString(value);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

}
