package br.com.arquitetura.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class BeanUtils extends org.apache.commons.beanutils.BeanUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(BeanUtils.class);

	public static String beanAlias(final Class<?> clazz) {
		return StringUtils.uncapitalize(clazz.getSimpleName());
	}

	public static Field getDeclaredField(final Object bean, final String fieldName) throws NoSuchFieldException {
		return getDeclaredField(bean.getClass(), fieldName);
	}

	public static Field getDeclaredField(final Class<?> clazz, final String fieldName) throws NoSuchFieldException {
		for (Class superClass = clazz; superClass != Object.class;) {
			try {
				return superClass.getDeclaredField(fieldName);
			} catch (final NoSuchFieldException localNoSuchFieldException) {
				superClass = superClass.getSuperclass();
			}

		}

		throw new NoSuchFieldException("No such field: " + clazz.getName() + '.' + fieldName);
	}

	public static Object getFieldValue(final Object bean, final String fieldName) {
		Object result = null;
		try {
			final Field field = getDeclaredField(bean, fieldName);
			makeAccessible(field);
			result = field.get(bean);
		} catch (final IllegalAccessException illegalAccessException) {
			LOGGER.error("{}", illegalAccessException.getMessage());
		} catch (final NoSuchFieldException noSuchFieldException) {
			LOGGER.error("Campo [{}] não encontrado no alvo [{}]: {}", fieldName, bean, noSuchFieldException.getMessage());
		}
		return result;
	}

	public static void setFieldValue(final Object bean, final String fieldName, final Object value) {
		try {
			final Field field = getDeclaredField(bean, fieldName);
			makeAccessible(field);
			field.set(bean, value);
		} catch (final IllegalAccessException illegalAccessException) {
			LOGGER.error("{}", illegalAccessException.getMessage());
		} catch (final NoSuchFieldException noSuchFieldException) {
			LOGGER.error("Campo [{}] não encontrado no alvo [{}]: {}", fieldName, bean, noSuchFieldException.getMessage());
		}
	}

	public static void setFieldNumberValue(final Object bean, final String fieldName, final Object value) {
		try {
			final Field field = getDeclaredField(bean, fieldName);
			makeAccessible(field);
			final Class clazz = field.getType();
			final Number number = NumberUtils.createNumber(value.toString());
			if (Long.class.equals(clazz)) {
				try {
					field.setLong(bean, new Long(number.longValue()).longValue());
				} catch (final IllegalArgumentException iar) {
					field.set(bean, new Long(number.longValue()));
				}
			} else if (Integer.class.equals(clazz)) {
				try {
					field.setInt(bean, number.intValue());
				} catch (final IllegalArgumentException iar) {
					field.set(bean, new Integer(number.intValue()));
				}
			} else if (Short.class.equals(clazz)) {
				try {
					field.setShort(bean, new Short(number.shortValue()).shortValue());
				} catch (final IllegalArgumentException iar) {
					field.set(bean, Short.valueOf(number.shortValue()));
				}
			} else {
				field.set(bean, value);
			}
		} catch (final IllegalAccessException illegalAccessException) {
			LOGGER.error("{}", illegalAccessException.getMessage());
		} catch (final NoSuchFieldException e) {
			e.printStackTrace();
		}
	}

	public static Map describeByDbField(final Object bean, final boolean isLower, final boolean isLine) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		final Map description = new HashMap();
		if (bean == null) {
			return description;
		}
		final PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(bean);
		for (final PropertyDescriptor descriptor : descriptors) {
			final String name = descriptor.getName();
			if (PropertyUtils.getReadMethod(descriptor) != null) {
				String newName = null;
				if (isLower) {
					if (isLine) {
						newName = getDbFieldName(name).toLowerCase();
					} else {
						newName = name.toLowerCase();
					}
				} else if (isLine) {
					newName = getDbFieldName(name).toUpperCase();
				} else {
					newName = name.toUpperCase();
				}

				description.put(newName, PropertyUtils.getProperty(bean, name));
			}
		}
		return description;
	}

	public static String getDbFieldName(final String name) {
		final StringBuffer strb = new StringBuffer();
		for (int i = 0; i < name.length(); i++) {
			final char ch = name.charAt(i);
			if (Character.isUpperCase(ch)) {
				strb.append("_");
				strb.append(Character.toLowerCase(ch));
			} else {
				strb.append(ch);
			}
		}
		return strb.toString();
	}

	public static Object invokeGetterMethod(final Object bean, final String propertyName) {
		final String getterMethodName = "get" + StringUtils.capitalize(propertyName);
		return invokeMethod(bean, getterMethodName, new Class[0], new Object[0]);
	}

	public static void invokeSetterMethod(final Object bean, final String propertyName, final Object value) {
		invokeSetterMethod(bean, propertyName, value, null);
	}

	public static void invokeSetterMethod(final Object bean, final String propertyName, final Object value, final Class<?> propertyType) {
		Class type;
		try {
			type = propertyType != null ? propertyType : bean.getClass().getDeclaredField(propertyName).getType();
		} catch (final Exception e) {
			type = value.getClass();
		}
		final String setterMethodName = "set" + StringUtils.capitalize(propertyName);
		invokeMethod(bean, setterMethodName, new Class[] { type }, new Object[] { value });
	}

	public static Object invokeMethod(final Object bean, final String methodName, final Class<?>[] parameterTypes, final Object[] parameters) {
		final Method method = getDeclaredMethod(bean, methodName, parameterTypes);
		if (method == null) {
			throw new IllegalArgumentException("Could not find method [" + methodName + "] on target [" + bean + "]");
		}
		method.setAccessible(true);
		try {
			return method.invoke(bean, parameters);
		} catch (final Exception e) {
			throw convertReflectionExceptionToUnchecked(e);
		}
	}

	public static void makeAccessible(final Field field) {
		if ((!Modifier.isPublic(field.getModifiers())) || (!Modifier.isPublic(field.getDeclaringClass().getModifiers()))) {
			field.setAccessible(true);
		}
	}

	protected static Method getDeclaredMethod(final Object bean, final String methodName, final Class<?>[] parameterTypes) {
		for (Class superClass = bean.getClass(); superClass != Object.class;) {
			try {
				return superClass.getDeclaredMethod(methodName, parameterTypes);
			} catch (final NoSuchMethodException localNoSuchMethodException) {
				superClass = superClass.getSuperclass();
			}

		}

		return null;
	}

	public static <T> Class<T> getSuperClassGenricType(final Class<?> clazz) {
		return getSuperClassGenricType(clazz, 0);
	}

	public static Class getSuperClassGenricType(final Class<?> clazz, final int index) {
		final Type genType = clazz.getGenericSuperclass();
		if (!(genType instanceof ParameterizedType)) {
			LOGGER.warn("Superclasse de {} não é um ParameterizedType", clazz.getSimpleName());
			return Object.class;
		}
		final Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		if ((index >= params.length) || (index < 0)) {
			LOGGER.warn("Índice: {}, Tamanho do Tipo Parametrizado {}: {}", index, clazz.getSimpleName(), params.length);
			return Object.class;
		}
		if (!(params[index] instanceof Class)) {
			LOGGER.warn("{} not set the actual class on superclass generic parameter", clazz.getSimpleName());
			return Object.class;
		}
		return (Class) params[index];
	}

	public static List convertElementPropertyToList(final Collection collection, final String propertyName) {
		final List list = new ArrayList();
		try {
			for (final Iterator localIterator = collection.iterator(); localIterator.hasNext();) {
				final Object obj = localIterator.next();
				list.add(PropertyUtils.getProperty(obj, propertyName));
			}
		} catch (final Exception e) {
			throw convertReflectionExceptionToUnchecked(e);
		}
		return list;
	}

	public static String convertElementPropertyToString(final Collection collection, final String propertyName, final String separator) {
		final List list = convertElementPropertyToList(collection, propertyName);
		return StringUtils.join(list, separator);
	}

	public static Object convertStringToObject(final String value, final Class<?> toType) {
		try {
			return ConvertUtils.convert(value, toType);
		} catch (final Exception e) {
			throw convertReflectionExceptionToUnchecked(e);
		}
	}

	public static RuntimeException convertReflectionExceptionToUnchecked(final Exception e) {
		if (((e instanceof IllegalAccessException)) || ((e instanceof IllegalArgumentException)) || ((e instanceof NoSuchMethodException))) {
			return new IllegalArgumentException("Reflection Exception.", e);
		}
		if ((e instanceof InvocationTargetException)) {
			return new RuntimeException("Reflection Exception.", ((InvocationTargetException) e).getTargetException());
		}
		if ((e instanceof RuntimeException)) {
			return (RuntimeException) e;
		}
		return new RuntimeException("Unexpected Checked Exception.", e);
	}

	public static Class<?> getClazzAttribute(final Class<?> clazz, final String propertyPath) {
		try {
			final String[] paths = propertyPath.split("\\.");
			Class classeAtual = clazz;
			for (final String path : paths) {
				Field f;
				try {
					f = classeAtual.getDeclaredField(path);
				} catch (final NoSuchFieldException e) {
					f = classeAtual.getSuperclass().getDeclaredField(path);
				}
				classeAtual = f.getType();
			}
			return classeAtual;
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
