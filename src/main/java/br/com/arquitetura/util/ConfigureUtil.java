package br.com.arquitetura.util;

/**
 * Busca o caminho onde está setado o arquivo de jobs.
 * 
 * @author Sérgio
 * 
 */
public class ConfigureUtil {

	protected static final String getPathFileConf() {
		String path = "";
		if (!path.endsWith("/") || !path.endsWith("\\")) {
			path += "/";
		}
		return path;
	}

	protected static final String getLine(final String line, final int pos) {
		return line.substring(pos + 1).trim();
	}

	protected static final String getLine(final String line, final int init,
			final int fim) {
		return line.substring(init, fim).trim();
	}

}
