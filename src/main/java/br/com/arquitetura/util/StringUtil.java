package br.com.arquitetura.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

/**
 * String Utility Class This is used to encode passwords programmatically
 * 
 * <p>
 * <a h ref="StringUtil.java.html"><i>View Source</i></a>
 * </p> 
 * 
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 * @version $Revision: 1.1 $ $Date: 2004/02/03 04:56:21 $
 */
public class StringUtil {

	public static String encode(String str) {
		byte[] bytesEncoded = Base64.encodeBase64(str.getBytes());

		return new String(bytesEncoded);
	}

	public static String decode(String str) {
		byte[] bytesDecoded = Base64.decodeBase64(str.getBytes());

		return new String(bytesDecoded);
	}

	public static String encodePassword(String s) {
		return encode(s);
	}

	public static String decodePassword(String s) {
		return decode(s);
	}

	@SuppressWarnings("rawtypes")
	public static Object buscarValor(String xml, String no, Class clazz) {
		if (xml != null && no != null && xml.contains("<" + no + ">")) {
			String s = xml.substring(xml.indexOf("<" + no + ">")
					+ no.toString().length() + 2, xml.indexOf("</" + no + ">"));
			if (Boolean.class == clazz) {
				return Boolean.valueOf(s);
			} else if (Integer.class == clazz) {
				return new Integer(s);
			} else if (Long.class == clazz) {
				return new Long(s);
			} else if (String.class == clazz) {
				return s;
			}
		}
		return null;
	}

	private static Map<String, String> caracteresEspeciaisMap;
	static {
		caracteresEspeciaisMap = new HashMap<String, String>();
		caracteresEspeciaisMap.put("�", "a");
		caracteresEspeciaisMap.put("�", "A");
		caracteresEspeciaisMap.put("�", "a");
		caracteresEspeciaisMap.put("�", "A");
		caracteresEspeciaisMap.put("�", "a");
		caracteresEspeciaisMap.put("�", "A");
		caracteresEspeciaisMap.put("�", "a");
		caracteresEspeciaisMap.put("�", "A");
		caracteresEspeciaisMap.put("�", "e");
		caracteresEspeciaisMap.put("�", "E");
		caracteresEspeciaisMap.put("�", "e");
		caracteresEspeciaisMap.put("�", "E");
		caracteresEspeciaisMap.put("�", "i");
		caracteresEspeciaisMap.put("�", "I");
		caracteresEspeciaisMap.put("�", "i");
		caracteresEspeciaisMap.put("�", "I");
		caracteresEspeciaisMap.put("�", "i");
		caracteresEspeciaisMap.put("�", "I");
		caracteresEspeciaisMap.put("�", "o");
		caracteresEspeciaisMap.put("�", "O");
		caracteresEspeciaisMap.put("�", "o");
		caracteresEspeciaisMap.put("�", "O");
		caracteresEspeciaisMap.put("�", "o");
		caracteresEspeciaisMap.put("�", "O");
		caracteresEspeciaisMap.put("�", "o");
		caracteresEspeciaisMap.put("�", "O");
		caracteresEspeciaisMap.put("�", "u");
		caracteresEspeciaisMap.put("�", "U");
		caracteresEspeciaisMap.put("�", "u");
		caracteresEspeciaisMap.put("�", "U");
		caracteresEspeciaisMap.put("�", "c");
		caracteresEspeciaisMap.put("�", "C");
		caracteresEspeciaisMap.put("�", "n");
		caracteresEspeciaisMap.put("�", "N");
		caracteresEspeciaisMap.put("�", "");
		caracteresEspeciaisMap.put("�", "");
		caracteresEspeciaisMap.put("�", "");
		caracteresEspeciaisMap.put("�", "");
		caracteresEspeciaisMap.put("�", "");
		caracteresEspeciaisMap.put("�", "");
		caracteresEspeciaisMap.put("�", "");
		caracteresEspeciaisMap.put("�", "");
		caracteresEspeciaisMap.put("�", "");

	}

	String substituirAcentos(String texto) {
		for (String acento : caracteresEspeciaisMap.keySet()) {
			if (texto.contains(acento)) {
				texto = texto.replaceAll(acento,
						caracteresEspeciaisMap.get(acento));
			}
		}
		return texto;
	}

	private static String acentuado = "���������������������������������������������������";
	private static String semAcento = "cCaeiouyAEIOUYaeiouAEIOUaonaeiouyAEIOUAONaeiouAEIOU";
	private static char[] tabela;

	static {
		tabela = new char[256];
		for (int i = 0; i < tabela.length; ++i) {
			tabela[i] = (char) i;
		}
		for (int i = 0; i < acentuado.length(); ++i) {
			tabela[acentuado.charAt(i)] = semAcento.charAt(i);
		}
	}

	public static String removerAcentos(final String s) {

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); ++i) {
			char ch = s.charAt(i);
			if (ch < 256) {
				sb.append(tabela[ch]);
			} else {
				sb.append(ch);
			}
		}
		return sb.toString();
	}

	private static List<Character> caracteresPermitidos;
	static {
		caracteresPermitidos = new ArrayList<Character>();

		caracteresPermitidos.add('-');
		caracteresPermitidos.add('a');
		caracteresPermitidos.add('b');
		caracteresPermitidos.add('c');
		caracteresPermitidos.add('d');
		caracteresPermitidos.add('e');
		caracteresPermitidos.add('f');
		caracteresPermitidos.add('g');
		caracteresPermitidos.add('h');
		caracteresPermitidos.add('i');
		caracteresPermitidos.add('j');
		caracteresPermitidos.add('k');
		caracteresPermitidos.add('l');
		caracteresPermitidos.add('m');
		caracteresPermitidos.add('n');
		caracteresPermitidos.add('o');
		caracteresPermitidos.add('p');
		caracteresPermitidos.add('q');
		caracteresPermitidos.add('r');
		caracteresPermitidos.add('s');
		caracteresPermitidos.add('t');
		caracteresPermitidos.add('u');
		caracteresPermitidos.add('v');
		caracteresPermitidos.add('x');
		caracteresPermitidos.add('y');
		caracteresPermitidos.add('w');
		caracteresPermitidos.add('z');
		caracteresPermitidos.add('0');
		caracteresPermitidos.add('1');
		caracteresPermitidos.add('2');
		caracteresPermitidos.add('3');
		caracteresPermitidos.add('4');
		caracteresPermitidos.add('5');
		caracteresPermitidos.add('6');
		caracteresPermitidos.add('7');
		caracteresPermitidos.add('8');
		caracteresPermitidos.add('9');
	}

	public static String excluirCaracteresNaoPermitidos(String texto) {
		if (texto != null) {

			List<String> paraRetirar = new ArrayList<String>();
			for (int i = 0; i < texto.length(); i++) {
				Character c = texto.charAt(i);

				if (!caracteresPermitidos.contains(c)) {
					paraRetirar.add(c.toString());
				}
			}

			for (String s : paraRetirar) {
				texto = texto.replaceAll(s, "");
			}
		}
		return texto;
	}

	public static String substituirCaracteresEspeciais(String texto) {
		for (String c : caracteresEspeciaisMap.keySet()) {
			if (texto.contains(c)) {
				texto = texto.replaceAll("\\" + c,
						caracteresEspeciaisMap.get(c));
			}
		}
		return texto;
	}

	private static final String enderecoUrlRegex = "[\\|\\/\\\\\\s?!,.:;=_<>'\"%$()\\[\\]{}-]+";

	public static String ajustarTextoParaUrl(String texto) {
		texto = texto.toLowerCase();

		texto = substituirCaracteresEspeciais(texto);

		texto = texto.replaceAll(enderecoUrlRegex, "-");

		if (texto != null) {
			if (texto.startsWith("-")) {
				texto = texto.substring(1);
			}
			if (texto.endsWith("-")) {
				texto = texto.substring(0, texto.length() - 1);
			}
		}

		texto = excluirCaracteresNaoPermitidos(texto);

		return texto;
	}

	public static Map<String, Object> getQueryMap(String query) {
		String[] params = query.split("&");
		Map<String, Object> map = new HashMap<String, Object>();
		for (String param : params) {
			if (param != null && param.trim().length() != 0) {
				String name = param.split("=")[0];
				String value = param.split("=")[1];
				map.put(name, value);
			}
		}
		return map;
	}
}