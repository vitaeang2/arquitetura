package br.com.arquitetura.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class ParameterBuilder implements Serializable {

	private List<Entry> entryList;

	private ParameterBuilder(String key, Object obj) {
		super();
		this.entryList = new ArrayList<Entry>();
		this.entryList.add(new Entry(key, obj));
	}

	public ParameterBuilder() {
		super();
		this.entryList = new ArrayList<Entry>();
	}

	public static ParameterBuilder start(String key, Object obj) {
		ParameterBuilder param = new ParameterBuilder(key, obj);
		return param;
	}

	public static ParameterBuilder start() {
		ParameterBuilder param = new ParameterBuilder();
		return param;
	}

	public ParameterBuilder more(String key, Object obj) {
		this.entryList.add(new Entry(key, obj));
		return this;
	}

	public List<Entry> getEntryList() {
		return this.entryList;
	}

	public class Entry {
		private String key;
		private Object value;

		protected Entry(String key, Object value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return this.key;
		}

		public Object getValue() {
			return this.value;
		}
	}
}
