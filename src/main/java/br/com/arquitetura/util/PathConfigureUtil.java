package br.com.arquitetura.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe de inicialização dos serviços a serem executados via jobs.
 * 
 * @author Sérgio
 * 
 */
public class PathConfigureUtil extends ConfigureUtil {

	public static String URL;

	private static final Logger LOGGER = LoggerFactory.getLogger(PathConfigureUtil.class);

	private static Properties configFile;

	public static void loadJobFile(final String name) {

		final String pathFileConf = getPathFileConf();
		String path = null;
		if (pathFileConf == null) {
			path = String.format("%s", name);
		} else {
			path = name;
		}
		configFile = new Properties();
		final File file = new File(path);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			configFile.load(fis);
		} catch (final IOException e) {
			LOGGER.error("Erro ao tentar carregar o arquivo de tarefas {}", file);
		}
	}

	public static void startJobs(String ctxPath) throws Exception {

		URL = configFile.getProperty("url").trim();
		URL += ctxPath;
	}

}
