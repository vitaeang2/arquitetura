package br.com.arquitetura.util;

import java.util.Date;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.SimpleTriggerImpl;

/**
 * <code>AgendadorUtil</code>
 *
 * Classe que manipula os serviços de jobs.
 *
 * @author Sérgio
 */
public class AgendadorUtil {

	private static Scheduler scheduler;

	@SuppressWarnings("unchecked")
	public static void agendarTarefa(final String expressao, final String nomeClasse) throws Exception {
		final JobDetail jobDetail = JobBuilder.newJob((Class<? extends Job>) Class.forName(nomeClasse)).withIdentity(nomeClasse).build();
		final Trigger trigger = TriggerBuilder.newTrigger().withIdentity(nomeClasse).withSchedule(CronScheduleBuilder.cronSchedule(expressao)).build();
		getScheduler().scheduleJob(jobDetail, trigger);
	}
	
	@SuppressWarnings("unchecked")
	public static void agendarTarefa(Date dataFim,	String nomeClasse, String id)
			throws Exception {
		JobDetail jobdetail = JobBuilder.newJob((Class<? extends Job>) Class.forName(nomeClasse)).withIdentity(nomeClasse+id).build();

		SimpleTriggerImpl trigger = new SimpleTriggerImpl();
		trigger.setName(nomeClasse+id);;
		trigger.setStartTime(dataFim);
		trigger.setRepeatCount(0);
		getScheduler().scheduleJob(jobdetail, trigger);
	}

	public static void desagendarTarefa(final Class<?> clazz, final String id) throws SchedulerException {
		getScheduler().unscheduleJob(TriggerKey.triggerKey(clazz.getName() + id));
	}

	public static boolean existeTarefa(final Class<?> clazz, final String id) throws SchedulerException {
		return getScheduler().checkExists(TriggerKey.triggerKey(clazz.getName() + id));
	}

	public static void interromperTarefa(final Class<?> clazz, final String id) throws Exception {
		getScheduler().interrupt(JobKey.jobKey(clazz.getName()+id));
	}

	protected static Scheduler getScheduler() throws SchedulerException {
		if (scheduler == null) {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
		}
		return scheduler;
	}

}
