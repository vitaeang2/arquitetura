package br.com.arquitetura.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe de inicialização dos serviços a serem executados via jobs.
 * 
 * @author Sérgio
 * 
 */
public class JobsConfigureUtil extends ConfigureUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(JobsConfigureUtil.class);

	private static Properties jobFile;

	private static final String JOBS_QUANTIDADE = "JOBS_QUANTIDADE";

	private static final String JOBS_ = "JOBS_";

	public static void loadJobFile(final String name) {
		final String pathFileConf = getPathFileConf();
		String path = null;
		if (pathFileConf == null) {
			path = String.format("%s", name);
		} else {
			path = name;
		}
		jobFile = new Properties();
		final File file = new File(path);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			jobFile.load(fis);
		} catch (final IOException e) {
			LOGGER.error("Erro ao tentar carregar o arquivo de tarefas {}", file);
		}
	}

	public static void startJobs() throws Exception {
		Integer cont = Integer.valueOf(jobFile.getProperty(JOBS_QUANTIDADE));

		for (; cont >= 1; cont--) {
			final String value = String.format("%s%s", JOBS_, cont.toString());
			String line = jobFile.getProperty(value).trim();
			int pos = line.indexOf("|");
			final String nameClass = getLine(line, 0, pos);
			line = line.substring(pos + 1);
			pos = line.indexOf("|");
			final String descricao = getLine(line, 1, pos);
			line = line.substring(pos + 1);
			pos = line.indexOf("|");
			final String expressao = getLine(line, 1, pos);
			final Boolean ativo = Boolean.valueOf(getLine(line, pos + 1));
			LOGGER.debug("Inicializando agendamento de tarefa...");
			LOGGER.debug("\t{}", nameClass);
			LOGGER.debug("\t{}", descricao);
			LOGGER.debug("\t{}", expressao);
			LOGGER.debug("\t{}", ativo);

			if (ativo) {
				AgendadorUtil.agendarTarefa(expressao, nameClass);
			}
			LOGGER.debug("Agendamento de tarefa concluído.");
		}
	}

}
