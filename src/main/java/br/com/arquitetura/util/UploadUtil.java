package br.com.arquitetura.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

import br.com.arquitetura.exception.NegocioException;

public class UploadUtil {

	public static void copyFile(File source, File destination)
			throws IOException {
		if (destination.exists())
			destination.delete();

		FileChannel sourceChannel = null;
		FileChannel destinationChannel = null;

		try {
			sourceChannel = new FileInputStream(source).getChannel();
			destinationChannel = new FileOutputStream(destination).getChannel();
			sourceChannel.transferTo(0, sourceChannel.size(),
					destinationChannel);
		} finally {
			if (sourceChannel != null && sourceChannel.isOpen())
				sourceChannel.close();
			if (destinationChannel != null && destinationChannel.isOpen())
				destinationChannel.close();
		}
	}

	public static void copyFile(String destino, String fileName, InputStream in) {
		try {

			File file = new File(destino);

			if (!file.exists() && !file.mkdirs()) {
				throw new NegocioException(
						"Caminho inexistente é não pode ser criado");
			}

			file = new File(destino + File.separator + fileName);

			OutputStream out = new FileOutputStream(file);

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}

			in.close();
			out.flush();
			out.close();
			System.out.println("Novo arquivo criado!");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (NegocioException e) {
			e.printStackTrace();
		}
	}

	public static boolean removeFile(String destino, String fileName) {
		File file = new File(destino + File.separator + fileName);
		return file.delete(); 
	}

	public static boolean removeFile(String caminhoImagens, String caminhoTipo,
			String titulo) {
		File file = new File(caminhoImagens + File.separator + caminhoTipo
				+ File.separator + titulo);

		return file.delete();
	}

}
