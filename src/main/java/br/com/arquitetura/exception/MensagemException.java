package br.com.arquitetura.exception;

import java.util.LinkedList;
import java.util.List;

public class MensagemException {

	/**
	 * Chave da mensagem que será mostrada para usuário ou chave da mensagem do arquivo properties
	 */
	private String chaveMensagem;

	/**
	 * Argumentos da mensagem, podendo ser o valor direto ou chave de um arquivo properties
	 */
	private String[] argumentos;
	
	/**
	 * Atributo que recebe o foco.
	 */
	private String atributoFoco;
	
	/**
	 * Abas
	 */
	private List<String> abas;

	/**
	 * Cria a exceção somente com mensagem.
	 * 
	 * @param chaveMensagem - Id da mensagem.
	 */
	public MensagemException(final String chaveMensagem) {
		this.chaveMensagem = chaveMensagem;
		this.abas = new LinkedList<String>();
	}

	/**
	 * 
	 * Cria a exceção para uma mensagem e adiciona os argumentos de argumento na mensagem
	 * 
	 * @param chaveMensagem - Define qual o id da mensagem que será apresentada.
	 * @param argumentos - Define o nome dos argumentos que receberão a mensagem.
	 */
	public MensagemException(final String chaveMensagem, final String... argumentos) {
		this(chaveMensagem);
		this.argumentos = argumentos;
	}

	/**
	 * 
	 * Retornar o id da mensagem a ser apresentada.
	 * 
	 * @return Descrição de identificação da mensagem a ser apresentada
	 */
	public String getChaveMensagem() {
		return this.chaveMensagem;
	}

	/**
	 * 
	 * Define o id da mensagem.
	 * 
	 * @param chaveMensagem descrição da identificação da mensagem
	 */
	public void setChaveMensagem(final String chaveMensagem) {
		this.chaveMensagem = chaveMensagem;
	}

	/**
	 * 
	 * Retorna os argumentos que serão validados.
	 * 
	 * @return os argumentos que serão validados
	 */
	public String[] getArgumentos() {
		return this.argumentos;
	}

	/**
	 * 
	 * Retorna os argumentos de mensagem.
	 * 
	 * @param argumentos Descrição dos argumentos da mensagem
	 */
	public void setArgumentos(final String[] argumentos) {
		this.argumentos = argumentos;
	}

	/**
	 * 
	 * Retorna o atributo que recebe o foco
	 *
	 * @return atributo
	 */
	public String getAtributoFoco() {
		return this.atributoFoco;
	}

	/**
	 * 
	 * Atribui o atributo que recebe o foco
	 *
	 * @param atributoFoco
	 */
	public void setAtributoFoco(final String atributoFoco) {
		this.atributoFoco = atributoFoco;
	}

	/**
	 * 
	 * Adiciona as abas do campo do foco.
	 *
	 * @return abas.
	 */
	public List<String> getAbas() {
		return this.abas;
	}

	/**
	 * 
	 * Define as abas de foco.
	 *
	 * @param abas
	 */
	public void setAbas(final List<String> abas) {
		this.abas = abas;
	}
	
	/**
	 * 
	 * Adicionar aba.
	 *
	 * @param aba
	 */
	public void adicionarAba(final String aba){
		if(this.abas == null || this.abas.isEmpty()){
			this.abas = new LinkedList<String>();
		}
		this.abas.add(aba);
	}
}