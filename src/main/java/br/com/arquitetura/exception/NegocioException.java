package br.com.arquitetura.exception;

import java.util.ArrayList;
import java.util.List;

import br.com.arquitetura.util.PropertiesUtil;

@SuppressWarnings("serial")
public class NegocioException extends Exception {

	private List<MensagemException> mensagens = new ArrayList<MensagemException>();
	private String redirecionamento;

	public NegocioException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public NegocioException(String msg) {
		super(msg);
	}

	public NegocioException(String msg, String[] parametros) {
		super(PropertiesUtil.getMessageResourceString(msg, parametros));
		// FacesCtxHolder.showMessage(msg, parametros, MsgType.TIPO.AVISO);
	}

	/**
	 * Define o valor do atributo redirecionamento.
	 * 
	 * 
	 * @param redirecionamento
	 *            A String com a chave para onde deve ser redirecionado quando
	 *            houver a exceção. Corresponde ao nome lógico para onde deve
	 *            ser redirecionada a visão quando houver a exceção.
	 */
	public void setRedirecionamento(final String redirecionamento) {
		this.redirecionamento = redirecionamento;
	}

	/**
	 * Retorna o valor do atributo redirecionamento.
	 * 
	 * @return redirecionamento
	 */
	public String getRedirecionamento() {
		return this.redirecionamento;
	}

	/**
	 * Retorna o valor do atributo mensagens.
	 * 
	 * @return mensagens
	 */
	public List<MensagemException> getMensagens() {
		return this.mensagens;
	}

	/**
	 * Define o valor do atributo mensagens.
	 * 
	 * @param mensagens
	 *            valor a ser atribuído
	 */
	public void setMensagens(final List<MensagemException> mensagens) {
		this.mensagens = mensagens;
	}
}