package br.com.arquitetura.repository;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.transform.ResultTransformer;
import org.omnifaces.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.exception.NegocioException;
import br.com.arquitetura.util.BeanUtils;

@SuppressWarnings({ "unused", "unchecked", "rawtypes" })
public class BeanTransformer implements ResultTransformer {

	private static final long serialVersionUID = 4230836302340616396L;

	private static final Logger LOGGER = LoggerFactory.getLogger(BeanTransformer.class);

	private String hql;

	private String[] arrayAlias;

	private Object[] arrayValue;

	private String separator;

	private String forClazz;

	private final Class<?> clazz;

	private final boolean distinct;

	private final Map<Object, Object> mapTupleBean = new HashMap();

	private static Map<String, Object> mapInnerBean = new HashMap();

	private static boolean acessores;

	private static final String MSG = "Você está tentando acessar a classe br.com.entity.hibernate.BeanTransformer.BeanTransformer(.... ) para converter o resultado de uma consulta projeta com mais de 3 colunas selecionadas. Para teste é permitido somente 3. Caso deseje utilizar consultas com número ilimitado de colunas entre em contado através do email serginhoplf@gmail.com";

	public BeanTransformer(final Class<?> clazz) {
		this(clazz, false, true);
	}

	public BeanTransformer(final String hql) throws DaoException {
		this(hql, null);
	}

	public BeanTransformer(final String hql, final String forClazz) throws DaoException {
		this(clazzForHql(hql), hql.toLowerCase().contains("distinct"), true);
		this.forClazz = forClazz;
		this.hql = hql;
		this.arrayAlias = mountPathProjection();
		this.separator = ".";
	}

	public BeanTransformer(final Class<?> clazz, final boolean distinct, final boolean acessores) {
		this.distinct = distinct;
		this.clazz = clazz;
		BeanTransformer.acessores = acessores;
		LOGGER.info("Critérios: Converter o ResultSet para classe {} com elementos {} distintos {} utilização dos seus métodos assessores", clazz, (distinct ? "SOMENTE" : "NÃO"), (acessores ? "COM" : "SEM"));
	}

	public List transformList(List collection) {
		if (this.distinct) {
			final int size = collection.size();
			if (size > 1) {
				collection = new ArrayList(new HashSet(collection));
				LOGGER.info("Transformado: {} linhas encontradas para {} resultados distintos", size, collection.size());
			}
		}
		return collection;
	}

	public Object transformTuple(final Object[] values, final String[] aliases) {
		Object bean = null;
		try {
			if (values.length > 3) {
				throw new NegocioException("Você está tentando acessar a classe br.com.entity.hibernate.BeanTransformer.BeanTransformer(.... ) para converter o resultado de uma consulta projeta com mais de 3 colunas selecionadas. Para teste é permitido somente 3. Caso deseje utilizar consultas com número ilimitado de colunas, entre em contado através do email serginhoplf@gmail.com");
			}
			this.arrayValue = values;
			if (this.arrayAlias == null) {
				this.arrayAlias = aliases;
				this.separator = "_";
			}
			bean = newInstanceBean(values, this.arrayAlias);

			mapInnerBean.clear();
			for (int i = 0; i < values.length; i++) {
				if (values[i] != null) {
					final String alias = this.arrayAlias[i];
					final String[] aliasArray = alias.split("\\" + this.separator);

					processResult(bean, aliasArray, values[i]);
				}
			}
		} catch (final Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		return bean;
	}

	private void processResult(final Object bean, final String[] aliasArray, final Object value) {
		try {
			final int layerLength = aliasArray.length;
			if (1 == layerLength) {
				setValueTarget(bean, aliasArray[0], value);
			} else {
				final Object innerBean = currentInstanceInnerBean(bean, aliasArray[0]);
				processResult(innerBean, ArrayUtils.remove(aliasArray, 0), value);
			}
		} catch (final Exception exception) {
			LOGGER.error("Erro ao tentar processar o resultado:", exception);
		}
	}

	private Object currentInstanceInnerBean(final Object bean, final String fieldName) {
		Object innerBean = mapInnerBean.get(fieldName);
		if (innerBean == null) {
			innerBean = newIntanceInnerBean(bean, fieldName);
		}
		return innerBean;
	}

	public static Object newIntanceInnerBean(final Object bean, final String fieldName) {
		Object innerBean = null;
		try {
			final Field field = BeanUtils.getDeclaredField(bean, fieldName);
			BeanUtils.makeAccessible(field);
			innerBean = field.get(bean);
			if (innerBean == null) {
				if (field.getType() == Set.class) {
					innerBean = new HashSet();
				} else if (field.getType() == List.class) {
					innerBean = new ArrayList();
				} else {
					innerBean = field.getType().newInstance();
				}

				if ((innerBean instanceof Collection)) {
					BeanUtils.setFieldValue(bean, fieldName, innerBean);

					innerBean = newItemCollection(bean, field);
				} else {
					setValueTarget(bean, fieldName, innerBean);
				}

			} else if ((innerBean instanceof Collection)) {
				innerBean = newItemCollection(bean, field);
			}

			mapInnerBean.put(fieldName, innerBean);
		} catch (final Exception localException) {
		}
		return innerBean;
	}

	private Object newInstanceBean(final Object[] tuple, final String[] aliases) throws Exception {
		if (!this.distinct) {
			return targetClazz().newInstance();
		}
		final Object ID = serialID(tuple, aliases);
		Object objeto = this.mapTupleBean.get(ID);
		if (objeto == null) {
			objeto = targetClazz().newInstance();
			this.mapTupleBean.put(ID, objeto);
		}
		return objeto;
	}

	public Class<?> targetClazz() throws Exception {
		if (!Utils.isEmpty(this.forClazz)) {
			return BeanUtils.getClazzAttribute(this.clazz, this.forClazz);
		}
		return this.clazz;
	}

	private Object serialID(final Object[] tuple, final String[] aliases) {
		for (int i = 0; i < aliases.length; i++) {
			final String[] aliasArray = aliases[i].split("_");
			if ((1 == aliasArray.length) && ("id".equals(aliasArray[0].toLowerCase()))) {
				return tuple[i];
			}
		}
		return tuple[0];
	}

	private static void setValueTarget(final Object target, final String propertyName, final Object value) {
		if (acessores) {
			BeanUtils.invokeSetterMethod(target, propertyName, value);
		} else {
			BeanUtils.setFieldValue(target, propertyName, value);
		}
	}

	private static Object newItemCollection(final Object bean, final Field field) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
		final Class innerBeanClass = (Class) ((java.lang.reflect.ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
		final Object target = innerBeanClass.newInstance();

		final Method method = PropertyUtils.getPropertyDescriptor(bean, field.getName()).getReadMethod();
		final Object[] nulo = null;

		((Collection) method.invoke(bean, nulo)).add(target);
		return target;
	}

	private String[] mountPathProjection() throws DaoException {
		final String aliasClazz = aliasForClazz(this.hql);
		final String[] projecoes = projecoes();
		String pathTmp = "";
		final Map mapJoins = mapJoins();
		for (int i = 0; i < projecoes.length; i++) {
			pathTmp = propertyPath(projecoes[i], aliasClazz, mapJoins);
			if (!Utils.isEmpty(this.forClazz)) {
				final int pos = position(pathTmp, this.forClazz + ".", true);
				pathTmp = pathTmp.substring(pos);
			}
			projecoes[i] = pathTmp;
		}
		return projecoes;
	}

	private String[] projecoes() {
		int start = 0;
		if (Boolean.valueOf(this.distinct)) {
			start = position(this.hql, "distinct", true);
		} else {
			start = position(this.hql, "select", true);
		}
		final int stop = position(this.hql, "from", false);
		final String intervalo = this.hql.substring(start, stop).trim();
		return intervalo.replace(" ", "").replace("\t", "").split(",");
	}

	private Map<String, String> mapJoins() {
		final Map mapJoins = new HashMap();
		final String[] joinClazz = joinCalzz(this.hql);
		for (final String jc : joinClazz) {
			final String[] aux = jc.split("\\s");
			mapJoins.put(aux[(aux.length - 1)], aux[0]);
		}
		return mapJoins;
	}

	private static String[] joinCalzz(final String hql) {
		final int start = position(hql, "join", true);
		if (start == -1) {
			return new String[0];
		}
		int stop = position(hql, "where", false);
		if (stop == -1) {
			stop = position(hql, "order", false);
			if (stop == -1) {
				stop = hql.length();
			}
		}
		String joinProjection = hql.substring(start, stop).trim();
		joinProjection = replaceAllCase(joinProjection, 0, new String[] { "inner", "left", "join" }, new String[] { "", "", ";" });
		return joinProjection.split("\\;");
	}

	private String propertyPath(final String projection, final String aliasBean, final Map<String, String> mapJoins) {
		final String[] path = projection.split("\\.");
		if ((path.length > 1) && (mapJoins.containsKey(path[0]))) {
			String more = "";
			more = more + mapJoins.get(path[0]);
			more = more + ".";
			more = more + projection.replace(new StringBuilder(String.valueOf(path[0])).append(".").toString(), "");
			return propertyPath(more, aliasBean, mapJoins);
		}
		if (projection.contains(aliasBean)) {
			return projection.replace(aliasBean + ".", "");
		}
		return projection;
	}

	public static String replaceAllCase(final String txt, final int start, final String[] value, final String[] newValue) {
		final int pos = position(txt, value[start], false);
		if (pos != -1) {
			final StringBuilder sb = new StringBuilder();
			sb.append(txt.substring(0, pos).trim()).append(newValue[start]).append(txt.substring(pos + value[start].length(), txt.length()).trim());
			return replaceAllCase(sb.toString(), start, value, newValue);
		}
		if (start != (value.length - 1)) {
			return replaceAllCase(txt, start + 1, value, newValue);
		}
		return txt;
	}

	public static int position(final String hql, final String regex, final boolean isMore) {
		final int more = isMore ? regex.length() : 0;
		final int pos = hql.toLowerCase().indexOf(regex.toLowerCase());
		return pos == -1 ? -1 : pos + more;
	}

	public static Class<?> clazzForHql(final String hql) throws DaoException {
		try {
			final int index = hql.toLowerCase().lastIndexOf("from");
			String clazz = hql.substring(index + 4).trim();
			clazz = clazz.split("\\s")[0].trim();
			return Class.forName(clazz);
		} catch (final Exception e) {
			throw new DaoException("Não foi possível extrair a classe do hql: " + hql + " (" + e.getClass().getSimpleName() + ": " + e.getMessage() + ")", e);
		}
	}

	public static String aliasForClazz(final String hql) throws DaoException {
		final int start = position(hql, clazzForHql(hql).getName(), true);
		return hql.substring(start).trim().split("\\s")[0];
	}

	public static String aliasForJoin(final String hql, final String propertyPath) throws DaoException {
		final String[] joinClazz = joinCalzz(hql);
		final String[] property = propertyPath.split("\\.");
		String realPath = "";
		for (int p = property.length - 2; p >= 0; p--) {
			final String path = property[p];
			for (final String jc : joinClazz) {
				final String[] join = jc.split("\\s");
				if (join[0].contains(path)) {
					return join[(join.length - 1)] + (!Utils.isEmpty(realPath) ? "." + realPath.substring(0, realPath.length() - 1) : "");
				}
			}
			realPath = path + "." + realPath;
		}
		return aliasForClazz(hql) + "." + realPath.substring(0, realPath.length() - 1);
	}

	public String getForClazz() {
		return this.forClazz;
	}

	public Object[] getArrayValue() {
		return this.arrayValue;
	}

}
