package br.com.arquitetura.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.exception.DaoException;

/**
 * Interface de acesso ao dao, ela contem funcionalidades basicas de acesso ao
 * banco.
 * 
 * @author sergio
 * 
 */
public interface Dao extends Serializable {

	public Double sum(String hql) throws DaoException;

	public long count(Class<?> clazz) throws DaoException;

	public Entidade salvar(Entidade entidade);

	public boolean existe(Entidade entidade);

	public void remove(Entidade entidade);

	public <T extends Entidade> T atualizar(T entidade);

	public <T extends Entidade> T consultar(Class<T> clazz, Serializable id);

	public Entidade consultarGenerico(Class<?> clazz, Serializable id);

	public <T extends Entidade> T consultar(final String hql, Class<T> clazz);

	public void retirarObjetoSessao(Entidade value);

	public <T extends Entidade> List<T> listar(final String hql, Class<T> clazz);

	public <T extends Entidade> TypedQuery<T> createQuery(String hql, final Class<T> clazz);

	public <T extends Entidade> List<T> listaDemanda(final Class<T> clazz, String hql, final int startPage,
			final int maxResults, boolean sortOrder, String sortField);
	
	public <T extends Entidade> List<T> listarNativeQuery(final String hql, Class<?> class1);

	public long queryCount(String hql) throws DaoException;

	public boolean queryExists(String hql) throws DaoException;

	public Query queryProjection(String hql, String forClazz) throws DaoException;

	public <T> List<T> queryDemand(String hql, int first, int maxResults, String sortField, boolean sortOrder);

	public void flush();

	public Entidade consultarObjetoUsandoNativeQuery(String sql, Class<?> clazz);

	public <T extends Object> List<T> listarObject(final String hql, final Class<T> clazz);

	public Boolean existe(String sql);

}
