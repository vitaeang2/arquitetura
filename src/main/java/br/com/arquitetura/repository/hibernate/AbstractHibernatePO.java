package br.com.arquitetura.repository.hibernate;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.arquitetura.repository.Dao;

/**
 * Possui acesso ao Dao de serviço básico, classes PO deverão extender
 * 
 * @author Sérgio
 */
public class AbstractHibernatePO {

	public final static String UNCHECKED = "unchecked";

	@Autowired
	private Dao dao;

	protected Dao getDao() {
		return dao;
	}

	protected void setDao(final Dao dao) {
		this.dao = dao;
	}

}
