package br.com.arquitetura.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.exception.DaoException;

/**
 * Classe de acesso ao banco de dados. Possui servi�os basicos.
 * 
 * @author sergio
 * 
 */
@Repository
public class DaoImpl implements Dao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// protected static Logger logger = LoggerFactory.getLogger(DaoImpl.class);

	@PersistenceContext
	private EntityManager manager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#count(java.lang.Class)
	 */
	public long count(final Class<?> clazz) throws DaoException {
		final String hql = "select count(C.id) from " + clazz.getName() + " C";
		final TypedQuery<Number> query = manager.createQuery(hql, Number.class);
		final Number number = query.getSingleResult();
		return number.longValue();
	}

	public Double sum(String hql) throws DaoException {
		final TypedQuery<Number> query = manager.createQuery(hql, Number.class);
		final Number number = query.getSingleResult();
		if (number == null) {
			return new Double(0);
		}
		return number.doubleValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.arquitetura.repository.Dao#salvar(br.com.arquitetura.entidade.
	 * Entidade)
	 */
	public Entidade salvar(final Entidade entidade) {
		return manager.merge(entidade);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.arquitetura.repository.Dao#existe(br.com.arquitetura.entidade.
	 * Entidade)
	 */
	public boolean existe(final Entidade entidade) {
		return manager.contains(entidade);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.arquitetura.repository.Dao#remove(br.com.arquitetura.entidade.
	 * Entidade)
	 */
	public void remove(final Entidade entidade) {
		manager.remove(entidade);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.arquitetura.repository.Dao#atualizar(br.com.arquitetura.entidade
	 * .Entidade)
	 */
	@SuppressWarnings("unchecked")
	public Entidade atualizar(final Entidade entidade) {
		Entidade entidadeAtualizada = manager.merge(entidade);
		// manager.flush();
		// manager.clear();
		return entidadeAtualizada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#consultar(java.lang.Class,
	 * java.io.Serializable)
	 */
	public <T extends Entidade> T consultar(final Class<T> clazz, final Serializable id) {
		try {
			return manager.find(clazz, id);
		} catch (NoResultException nre) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#consultar(java.lang.Class,
	 * java.io.Serializable)
	 */
	public Entidade consultarGenerico(final Class<?> clazz, final Serializable id) {
		try {
			return (Entidade) manager.find(clazz, id);
		} catch (NoResultException nre) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#consultar(java.lang.String)
	 */
	public <T extends Entidade> T consultar(final String hql, final Class<T> clazz) {
		try {
			return manager.createQuery(hql, clazz).getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.arquitetura.repository.Dao#retirarObjetoSessao(java.lang.Object)
	 */
	public void retirarObjetoSessao(final Entidade entidade) {
		manager.detach(entidade);
	}

	public <T extends Entidade> TypedQuery<T> createQuery(String hql, final Class<T> clazz) {
		return manager.createQuery(hql, clazz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#listar(java.lang.String)
	 */
	public <T extends Entidade> List<T> listar(final String hql, final Class<T> clazz) {
		return manager.createQuery(hql, clazz).getResultList();
	}

	public <T extends Object> List<T> listarObject(final String hql, final Class<T> clazz) {
		return manager.createQuery(hql, clazz).getResultList();
	}

	public <T extends Entidade> List<T> listarNativeQuery(final String hql, Class<?> class1) {
		return manager.createNativeQuery(hql, class1).getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#listaDemanda(java.lang.Class,
	 * java.lang.String, int, int)
	 */
	public <T extends Entidade> List<T> listaDemanda(final Class<T> clazz, String hql, final int startPage,
			final int maxResults, boolean sortOrder, String sortField) {

		if (sortField != null) {
			final String orderType = sortOrder ? "DESC" : "ASC";
			hql += " order by " + sortField + " " + orderType;
		}

		final TypedQuery<T> query = manager.createQuery(hql, clazz);
		query.setFirstResult(startPage * maxResults);
		query.setMaxResults(maxResults);
		return query.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#queryCount(java.lang.String)
	 */
	public long queryCount(final String hql) throws DaoException {
		final TypedQuery<Number> query = manager.createQuery(hql, Number.class);
		final Number number = query.getSingleResult();
		return number.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#queryExists(java.lang.String)
	 */
	public boolean queryExists(final String hql) throws DaoException {
		return manager.createQuery(hql, Boolean.class).getSingleResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#queryProjection(java.lang.String,
	 * java.lang.String)
	 */
	public Query queryProjection(final String hql, final String forClazz) throws DaoException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#queryDemand(java.lang.String, int,
	 * int, java.lang.String, boolean)
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> queryDemand(String hql, final int first, final int maxResults, final String sortField,
			final boolean sortOrder) {

		TypedQuery<T> query;
		try {
			final String orderType = sortOrder ? "ASC" : "DESC";
			hql += " order by " + sortField + " " + orderType;

			query = (TypedQuery<T>) manager.createQuery(hql, BeanTransformer.clazzForHql(hql));

			query.setFirstResult(first);
			query.setMaxResults(maxResults);
			return query.getResultList();
		} catch (final DaoException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.repository.Dao#flush()
	 */
	public void flush() {
		manager.flush();
	}

	public Entidade consultarObjetoUsandoNativeQuery(final String sql, final Class<?> clazz) {
		try {
			final Query query = (Query) manager.createNativeQuery(sql, clazz);
			return (Entidade) query.getSingleResult();
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Boolean existe(String hql) {
		final Query query = (Query) manager.createQuery(hql);
		return ((Long) query.getSingleResult() > 0);
	}

}
