package br.com.arquitetura.web.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.arquitetura.model.Response;
import br.com.vitae.jwt.JWT;
import io.jsonwebtoken.ExpiredJwtException;

public class BaseController<E> {

	protected void validarToken(String token) throws Exception {

		try {
			JWT.parseJWT(token);
		} catch (ExpiredJwtException e) {
			throw new Exception(e.getMessage());
		}
	}

	protected Response enviarSucesso() {

		return new Response(true);
	}

	protected Response enviarFalha(String msg) {

		return new Response(false, msg);
	}

	protected <T extends Object> Response enviarSucesso(T objetoRetorno) {

		return new Response<T>(true, objetoRetorno);
	}

	protected <T extends Object> T retornar(Class<T> clazz, String str) {

		return new Gson().fromJson(str, clazz);
	}

	protected <T extends Object> T retornar(String str, TypeToken<T> tt) {

		return new Gson().fromJson(str, tt.getType());
	}
}
