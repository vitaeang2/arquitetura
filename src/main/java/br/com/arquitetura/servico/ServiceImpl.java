package br.com.arquitetura.servico;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.exception.DaoException;

@Repository
public class ServiceImpl<T extends Entidade> extends Business implements IService<T> {

	private static final long serialVersionUID = 1L;

	/**
	 * Pode ser usada para validações antes de salvar
	 * 
	 * @param entity
	 * @throws Exception
	 */
	protected void beforeSave(T entity) {

	};

	/**
	 * Pode ser usada para validações depois de salvar
	 * 
	 * @param entity
	 */
	protected void afterSave(T entity) {

	};

	@Transactional
	public List<T> listaDemanda(Class<T> clazz, String hql, int first, int startPage) {

		return getPersistencia().listaDemanda(clazz, hql, first, startPage, false, "id");
	}

	@Transactional
	public List<T> listaDemanda(Class<T> clazz, String hql, int first, int startPage, boolean sortOrder, String sortField) {

		return getPersistencia().listaDemanda(clazz, hql, first, startPage, sortOrder, sortField);
	}

	public void retiraObjetoSessao(Entidade value) {

		getPersistencia().retirarObjetoSessao(value);
	}

	@SuppressWarnings("unchecked")
	public T consultarObjeto(String hql, Class<Entidade> clazz) {

		return (T) getPersistencia().consultar(hql, clazz);
	}

	@Transactional
	public Integer count(Class<?> clazz) throws DaoException {

		return Integer.valueOf((int) getPersistencia().count(clazz));
	}

	@Transactional(readOnly = false)
	public synchronized Entidade salvarOuAtualizar(T entidade) {

		beforeSave(entidade);
		entidade = getPersistencia().atualizar(entidade);
		afterSave(entidade);

		return entidade;

	}

	@Transactional
	public T consultar(Class<T> clazz, Serializable id) {

		return (T) getPersistencia().consultar(clazz, id);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void remover(T entidade) {

		getPersistencia().remove(entidade);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.sisos.negocio.ctr.Service#listar()
	 */
	@SuppressWarnings("hiding")
	@Transactional
	public <T extends Entidade> List<T> listar(Class<T> clazz, String labelName) {

		return getPersistencia().listar("select new " + clazz.getName() + "(C.id, C." + labelName + ") from " + clazz.getName() + " C", clazz);
	}

	protected void validaCampoEspaco(String campo, String excecao) throws DaoException {

		if (campo == null || campo.trim().equals("")) {
			throw new DaoException(excecao);
		}
	}

}
