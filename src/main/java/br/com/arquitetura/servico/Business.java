package br.com.arquitetura.servico;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.arquitetura.repository.Dao;

/**
 * Classe a ser extendida por todas classes de negocio do sistema pois com ela
 * se tem acesso as funcionalidades basicas em banco de dados.
 * 
 * @author sergio
 * 
 */
public class Business {

	@Autowired
	private Dao dao;

	protected Dao getPersistencia() {
		return dao;
	}
}