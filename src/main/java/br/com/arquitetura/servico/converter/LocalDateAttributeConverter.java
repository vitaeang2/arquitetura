package br.com.arquitetura.servico.converter;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.arquitetura.util.DataUtil;

@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {

	@Override
	public Date convertToDatabaseColumn(LocalDate locDate) {

		return DataUtil.localDateToDate(locDate);
	}

	@Override
	public LocalDate convertToEntityAttribute(Date data) {

		return DataUtil.dateToLocalDate(data);
	}
}
