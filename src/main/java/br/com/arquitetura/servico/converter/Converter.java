package br.com.arquitetura.servico.converter;

import java.io.Serializable;
import java.util.List;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.model.BaseDTO;

public interface Converter<S extends Serializable, DTO extends BaseDTO, E extends Entidade<S>> {

	E dto2Entidade(DTO dto);

	DTO entidade2Dto(E entidade);

	List<DTO> listEntidade2ListDto(List<E> listaEntidade);
}
