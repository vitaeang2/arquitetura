package br.com.arquitetura.servico;

import java.io.Serializable;
import java.util.List;

import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.exception.DaoException;

public interface IService<T> extends Serializable {

	public void retiraObjetoSessao(Entidade value);

	public List<T> listaDemanda(Class<T> clazz, String hql, int first, int startPage);

	public List<T> listaDemanda(Class<T> clazz, String hql, int first, int startPage, boolean sortOrder,
			String sortField);

	public T consultarObjeto(String hql, Class<Entidade> clazz);

	public Integer count(Class<?> clazz) throws DaoException;

	public Entidade salvarOuAtualizar(T entidade);

	public T consultar(Class<T> clazz, Serializable id);

	public void remover(T entidade);

	public <T extends Entidade> List<T> listar(Class<T> clazz, String labelName);

}