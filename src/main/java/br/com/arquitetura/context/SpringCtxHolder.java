package br.com.arquitetura.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import br.com.arquitetura.util.UtilObjeto;

public class SpringCtxHolder implements ApplicationContextAware {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCtxHolder.class);

	protected static ApplicationContext applicationContext;
	
	public void setApplicationContext(final ApplicationContext appCtx) throws BeansException {
		cleanApplicationContext();
		LOGGER.info("Inicializando o application Context do Spring...");

		SpringCtxHolder.applicationContext = appCtx;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(final String beanAlias) {
		return (T) getApplicationContext().getBean(beanAlias);
	}

	public static <T> T getBean(final Class<T> clazz) {
		final String beanAlias = UtilObjeto.beanAlias(clazz);
		return getBean(beanAlias);
	}

	public static String getRealPath() {
		final String path = getApplicationContext().getClassLoader().getResource("").getPath();
		return path.replace("/", "\\").substring(1, path.lastIndexOf("web") + 3);
	}

	public static void cleanApplicationContext() {
		applicationContext = null;
	}

}
