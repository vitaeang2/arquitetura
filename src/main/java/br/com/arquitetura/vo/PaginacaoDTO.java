package br.com.arquitetura.vo;

import java.util.List;

import br.com.arquitetura.model.BaseDTO;

/**
 * 
 * <p>
 * <b>Title:</b> PaginacaoDTO.java
 * </p>
 * 
 * <p>
 * <b>Description:</b>
 * </p>	
 * 	
 * <p>	
 * <b>Company: </b> B3 - Tecnlogia da Informacao
 * </p>	
 * 	
 * @author Sergio Filho - sergioadsf@gmail.com
 * @param <E>
 * 
 * @version 1.0.0
 */
@SuppressWarnings({ "serial" })
public class PaginacaoDTO<E extends BaseDTO> implements BaseDTO {

	private int paginaCorrente;
	private int totalConsulta;
	private long totalEntradas;
	private List<E> entradas;

	public int getPaginaCorrente() {
		return paginaCorrente;
	}

	public int getTotalConsulta() {
		return totalConsulta;
	}

	public long getTotalEntradas() {
		return totalEntradas;
	}

	public List<E> getEntradas() {
		return entradas;
	}

	public void setPaginaCorrente(int paginaCorrente) {
		this.paginaCorrente = paginaCorrente;
	}

	public void setTotalEntradas(long totalEntradas) {
		this.totalEntradas = totalEntradas;
	}

	public void setEntradas(List<E> entradas) {
		this.entradas = entradas;
		this.totalConsulta = this.entradas.size();
	}

}
