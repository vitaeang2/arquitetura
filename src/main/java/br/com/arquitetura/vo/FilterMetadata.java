package br.com.arquitetura.vo;

public class FilterMetadata {

	private String value;

	private String matchMode;

	public String getValue() {

		return value;
	}

	public String getMatchMode() {

		return matchMode;
	}

}
