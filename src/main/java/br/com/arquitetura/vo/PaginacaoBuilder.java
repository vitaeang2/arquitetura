package br.com.arquitetura.vo;

import java.util.Map;

import br.com.arquitetura.context.SpringCtxHolder;
import br.com.arquitetura.entidade.Entidade;
import br.com.arquitetura.exception.DaoException;
import br.com.arquitetura.model.BaseDTO;
import br.com.arquitetura.repository.Dao;
import br.com.arquitetura.repository.DaoImpl;
import br.com.arquitetura.servico.converter.Converter;

@SuppressWarnings({ "rawtypes", "serial" })
public class PaginacaoBuilder<E extends Entidade> implements BaseDTO {

	private static final String _ALIAS_ = " alias ";

	private static final String _AS_ = " as ";

	private static Dao dao;

	private transient boolean sortOrder;

	private Class<E> clazz;

	private String hql;

	private int startPage;

	private int maxResults;

	private PaginacaoDTO paginacao;

	private String sortField;

	private Converter converter;

	private Map<String, FilterMetadata> filter;

	private String alias;

	static {
		dao = SpringCtxHolder.getBean(DaoImpl.class);
	}

	private PaginacaoBuilder( Converter converter, Class<E> clazz, int startPage, int maxResults, Map<String, FilterMetadata> filter ) {
		this(converter, clazz, String.format("from %s", clazz.getName()), startPage, maxResults, filter);
		this.converter = converter;
	}

	private PaginacaoBuilder( Converter converter, Class<E> clazz, String hql, int startPage, int maxResults, Map<String, FilterMetadata> filter ) {
		super();
		this.converter = converter;
		this.clazz = clazz;
		this.hql = hql;
		this.startPage = startPage;
		this.maxResults = maxResults;
		this.filter = filter;

		paginacao = new PaginacaoDTO();
	}

	public static <E extends Entidade> PaginacaoBuilder create(Converter converter, Class<E> clazz, PaginacaoParams params) {

		return new PaginacaoBuilder<E>(converter, clazz, params.getPage(), params.getSize(), params.getFilter());
	}

	public static <E extends Entidade> PaginacaoBuilder create(Converter converter, Class<E> clazz, String hql, PaginacaoParams params) {
		PaginacaoBuilder pb = new PaginacaoBuilder<E>(converter, clazz, hql, params.getPage(), params.getSize(), params.getFilter());
		if(params.getSortField() != null && !params.getSortField().equals("")){
			pb.addSortField(params.getSortField());
			if(params.getSortOrder() != null && params.getSortOrder().equals("asc")){
				pb.addAscOrder();
			} else {
				pb.addDescOrder();
			}
		}
		return pb;
	}

	public PaginacaoBuilder addDescOrder() {

		sortOrder = true;
		return this;
	}

	public PaginacaoBuilder addAscOrder() {

		sortOrder = false;
		return this;
	}

	public PaginacaoBuilder addSortField(String sortField) {

		this.sortField = sortField;
		return this;

	}

	@SuppressWarnings("unchecked")
	public PaginacaoDTO search() throws DaoException {

		alias = "";
		if (hql.contains(_ALIAS_)) {
			int pos = hql.indexOf(_ALIAS_);
			alias = hql.substring(pos + _ALIAS_.length());
			alias = alias.substring(0, alias.indexOf(" "));
			hql = hql.replace(_ALIAS_, _AS_);
		}
		
		verificaFiltro();
		
		paginacao.setEntradas(converter.listEntidade2ListDto(dao.listaDemanda(clazz, hql, startPage, maxResults, sortOrder, sortField)));

		StringBuilder sb = new StringBuilder();
		sb.append("select count(");
		if (!alias.equals("")) {
			sb.append(alias);
			sb.append(".");
		}
		sb.append("id) ");
		sb.append(hql.substring(hql.indexOf("from")));
		paginacao.setTotalEntradas(dao.queryCount(sb.toString()));
		paginacao.setPaginaCorrente(++startPage);

		return paginacao;
	}

	private void verificaFiltro() {
		
		boolean containsWhere = this.hql.contains("where");

		if (filter != null) {

			// TODO - Melhor solução é resolver no cliente, porem não estou conseguindo tratar o filters?: {[s: string]: FilterMetadata;}; do PRIMEFACES pelo cliente
			// Voltar novamente
			boolean isExec = false;
			TipoFilter tipo = new TipoFilter(containsWhere);
			for (String campo : filter.keySet()) {
				FilterMetadata metadata = filter.get(campo);
				
				campo = alias == null || alias.equals("") ? campo : alias + "." + campo;
				
				tipo.and(campo, metadata);
				if (isExec) {
					continue;
				}
				isExec = metadata.getValue().length() >= 3; // verifica se foi informado um valor de no minimo três campos para pode consultar
			}
			if (isExec) {
				hql += tipo.getSql();
			}
		}
	}

}
