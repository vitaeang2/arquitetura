package br.com.arquitetura.vo;

public class TipoFilter {

	private static final String STARTS = "startsWith";

	@SuppressWarnings("unused")
	private static final String CONTAINS = "contains";

	private static final String ENDS = "endsWidth";

	private StringBuilder sql;

	public TipoFilter(boolean containsWhere) {
		sql = new StringBuilder();
		if (!containsWhere) {
			sql.append(" where 1 = 1");
		}
	}

	private void add(String campo, FilterMetadata metadata) {

		sql.append(campo).append(getType(metadata));
	}

	public TipoFilter and(String campo, FilterMetadata metadata) {

		sql.append(" and ");
		add(campo, metadata);
		return this;
	}

	public TipoFilter or(String campo, FilterMetadata metadata) {

		sql.append(" or ");
		add(campo, metadata);
		return this;
	}

	private static String getType(FilterMetadata metadata) {

		if (STARTS.equals(metadata.getMatchMode())) {
			return " like '" + metadata.getValue() + "%'";
		}

		if (ENDS.equals(metadata.getMatchMode())) {
			return " like '%" + metadata.getValue() + "'";
		}

		return " like '%" + metadata.getValue() + "%'";
	}

	public String getSql() {

		return sql.toString();
	}
}
