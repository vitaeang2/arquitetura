package br.com.arquitetura.vo;

import java.util.Map;

public class PaginacaoParams {

	private int page;

	private int size;

	private String sortField;

	private String sortOrder;

	private Map<String, FilterMetadata> filter;

	public int getPage() {

		return page;
	}

	public void setPage(int page) {

		this.page = page;
	}

	public int getSize() {

		if (size <= 0) {
			this.size = 1;
		}
		return size;
	}

	public void setSize(int size) {

		this.size = size;
	}

	public String getSortField() {

		return sortField;
	}

	public void setSortField(String sortField) {

		this.sortField = sortField;
	}

	public String getSortOrder() {

		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {

		this.sortOrder = sortOrder;
	}

	public Map<String, FilterMetadata> getFilter() {

		return filter;
	}

	public void setFilter(Map<String, FilterMetadata> filter) {

		this.filter = filter;
	}

}
