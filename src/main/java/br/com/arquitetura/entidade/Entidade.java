package br.com.arquitetura.entidade;

import java.io.Serializable;

import org.hibernate.proxy.HibernateProxyHelper;

/**
 * TO base
 * 
 * @author Sérgio
 */
public abstract class Entidade<S extends Serializable>  implements Serializable {

	private static final long serialVersionUID = -6879250781879980342L;

	public abstract S getId();

	public abstract void setId(S id);

	public abstract String getLabel();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != HibernateProxyHelper.getClassWithoutInitializingProxy(obj)) {
			return false;
		}
		final Entidade other = (Entidade) obj;
		if (getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!getId().equals(other.getId())) {
			return false;
		}
		return true;
	}

}
